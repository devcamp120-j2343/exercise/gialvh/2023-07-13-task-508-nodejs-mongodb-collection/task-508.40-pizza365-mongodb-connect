//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//tạo 1 đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const voucherModel = new Schema({
    _id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        required : true,
        unique : true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu :{
        type : String,
    }

});

//export schema ra model
module.exports = mongoose.model("voucher", voucherModel)