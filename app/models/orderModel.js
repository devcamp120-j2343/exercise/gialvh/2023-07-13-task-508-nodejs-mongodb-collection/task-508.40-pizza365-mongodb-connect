//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//tạo 1 đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const orderModel = new Schema({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true,
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },
    status: {
        type: String,
        required: true
    }


});

//export schema ra model
module.exports = mongoose.model("order", orderModel)