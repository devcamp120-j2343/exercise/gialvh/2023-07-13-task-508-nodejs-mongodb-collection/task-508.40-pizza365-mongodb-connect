//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//tạo 1 đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const userModel = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required : true,
    },
    email: {
        type: String,
        required: true,
        unique : true
    },
    address :{
        type : String,
        required : true
    },
    phone : {
        type : String,
        required: true,
        unique : true,
        orders : [
            {
                _id : mongoose.Types.ObjectId,
                ref : "order"
            }
            
        ]
    }

});

//export schema ra model
module.exports = mongoose.model("user", userModel)