//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//tạo 1 đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const drinkSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        required : true,
        unique : true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia :{
        type : Number,
        default : 0,
        required : true
    }

});

//export schema ra model
module.exports = mongoose.model("drink", drinkSchema)