//khai báo thư viện express
const express = require('express');

//khai báo router
const { drinkRouter } = require('./app/routes/drinkRouter');
const {voucherRouter} = require(`./app/routes/voucherRouter`);
const {userRouter} = require(`./app/routes/userRouter`);
const {orderRouter} = require(`./app/routes/orderRouter`);

//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo các models mongoose
const drinkModel = require('./app/models/drinkModel'); 
const voucherModel = require('./app/models/voucherModel');
const userModel = require('./app/models/userModel');
const orderModel = require('./app/models/orderModel');

//kết nối mongoDB
mongoose.connect(`mongodb://127.0.0.1:27017/CRUD_Pizza365`)
.then(()=>console.log("Connected to mongoDB"))
.catch(error => handleError(error));

//khai báo app
const app = express();

//khai báo cổng chạy
const port = 8000;

app.use(`/`,drinkRouter);
app.use(`/`,voucherRouter);
app.use(`/`,userRouter);
app.use(`/`,orderRouter);

//khởi động app
app.listen(port,()=>{
    console.log("App listen on port ", port);
})